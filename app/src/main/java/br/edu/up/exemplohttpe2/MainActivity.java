package br.edu.up.exemplohttpe2;

import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.MediaController;
import android.widget.VideoView;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

public class MainActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
  }

  EditText editText;
  public void onClickBaixarTexto(View v){

    editText = (EditText) findViewById(R.id.editText);

    BaixarTextoTask task = new BaixarTextoTask();
    task.execute();
  }

  public class BaixarTextoTask extends AsyncTask<String, String, String> {


    @Override
    protected String doInBackground(String... strings) {

      String texto = new String();
      try {
        URL url = new URL("https://drive.google.com/uc?export=view&id=0BwO0-hA9fDiUMTFVS1NkVVhjaFk");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        InputStream is = con.getInputStream();

        Scanner leitor = new Scanner(is);
        while(leitor.hasNext()){
          String linha = leitor.nextLine();
          texto += linha + "\n";
        }
        leitor.close();
        is.close();

      } catch (MalformedURLException e) {
        e.printStackTrace();
      } catch (IOException e) {
        e.printStackTrace();
      }
      return texto;
    }

    @Override
    protected void onPostExecute(String texto) {
      editText.setText(texto);
    }
  }

  public void onClickTocarMusica(View v){

    MediaPlayer mediaPlayer = new MediaPlayer();
    try {
      mediaPlayer.setDataSource("https://drive.google.com/uc?export=view&id=0BwO0-hA9fDiUV0FDVk9BWXAwOVU");
      mediaPlayer.prepare();
      mediaPlayer.start();

    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void onClickTocarVideo(View v){

    VideoView vv = (VideoView) findViewById(R.id.videoView);
    vv.setVideoPath("https://drive.google.com/uc?export=view&id=0BwO0-hA9fDiUZ2t5OTdUY3Z4ZEk");
    vv.setMediaController(new MediaController(this));
    vv.start();
  }
}











